class BoutonLib {
  static creerBouton(i, j){ // crée un bouton pour la position (i, j)
    let btn = document.createElement("button");
    $(btn).attr("class", "case base");
    $(btn).attr("onclick", "clicBtn(this)");
    $(btn).attr("id", i+"-"+j);
    $(btn).contextmenu( BoutonLib.contextmenuBtn );
    return btn;
  }

  static getBouton(x, y){ // retourne le bouton à la position (x, y)
    return $("#"+x+"-"+y);
  }

  static getEtat(btn){ // retourne l'état du bouton
    let classBtn = $(btn).attr("class");
    let listeClass = classBtn.split(" ");
    return listeClass[1];
  }

  static getPosition(bouton){ // retourne la position du bouton
    let pos = $(bouton).attr("id").split("-");
    return [Number(pos[0]), Number(pos[1])]
  }

  static contextmenuBtn(event){
    if ( BoutonLib.getEtat(event.target) == "base" ){ // si à l'état 1
      EtatsLib.setDrapeau(event.target);
    }
    else{
      EtatsLib.setBase(event.target);
    }
    return false;
  }
}


class EtatsLib{
  static setBombe(btn){
    $(btn).attr("class", "case bombe");
  }

  static setDesac(btn, val){
    let pos = BoutonLib.getPosition(btn);
    if(val==0){
      val = "";
    }
    $(btn).replaceWith( $(`<div class="case desac" id="${pos[0]}-${pos[1]}">${val}</div>`) );
    return ( --tableauBombes.nbCaseATeste == 0 );
  }

  static setBase(btn){
    // si on clique sur l'icon, retrouve le bouton
    if( $(btn).is("i") ){
      let parent = $(btn).parent()[0]
      let pos = BoutonLib.getPosition( parent );
      let newBtn = BoutonLib.creerBouton(pos[0], pos[1]);
      $(parent).replaceWith(newBtn);
    }
    else{
      let pos = BoutonLib.getPosition(btn);
      let newBtn = BoutonLib.creerBouton(pos[0], pos[1]);
      $(btn).replaceWith(newBtn);
    }
  }

  static setDrapeau(btn){ //flag
    let pos = BoutonLib.getPosition(btn);
    let balise = $(`<div id="${pos[0]}-${pos[1]}" class='case drapeau'><i class='material-icons'>flag</i></div>` );
    $(balise).contextmenu( BoutonLib.contextmenuBtn );
    $(btn).replaceWith(balise);
  }
}
