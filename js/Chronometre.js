class Chronometre{
  constructor(){
    this.temps = 0;
    this.id = null;
  }

  lancer(){
    this.id = window.setInterval(
      function(chrono){
        ++chrono.temps;
        $("#timeur").text(chrono.temps);
      },
      1000,
      chrono
    );

    console.log("deb", this);
  }

  stoper(){
    console.log("fin", this);
      window.clearTimeout(this.id);
      this.id = null;
  }
}

//temps = 0;
//idChrono = null;
/*
function lancer(){
  idChrono = setInterval(
    function(){
      $("#timeur").text( Number( $("#timeur").text() ) + 1 );
    },
    1000
  );
}

function stoper(){
  clearInterval(idChrono);
  idChrono = null;
}
*/
