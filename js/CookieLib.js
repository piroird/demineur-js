/*class CookieLib {
  // correspondance largeur du plateau et cookie
  static dicoCookies = new Map( [[9,"DEB"], [16,"INTE"], [30,"EXP"]] )

  static clearAll(){
    for(let [cle, _] of dicoCookies){
      Cookies.remove(cle);
    }
  }

  static majDebutant(newVal){
    Cookies.set(dicoCookies.get(9), newVal);
  }

  static majIntermediaire(newVal){
    Cookies.set(dicoCookies.get(16), newVal);
  }

  static majExpert(newVal){
    Cookies.set(dicoCookies.get(30), newVal);
  }
}*/

dicoCookies = new Map( [[9,"DEB"], [16,"INTE"], [30,"EXP"]] );

function clearAllCookies(){
  for(let [cle, _] of dicoCookies){
    Cookies.remove(cle, { path: '' });
  }
}

function majCookieDebutant(newVal){
  let nomCookie = dicoCookies.get(9);
  if( Cookies.get(nomCookie) == undefined || Cookies.get(nomCookie) > newVal )
    Cookies.set(nomCookie, newVal, { path: '' });
}

function majCookieIntermediaire(newVal){
  let nomCookie = dicoCookies.get(16);
  if( Cookies.get(nomCookie) == undefined || Cookies.get(nomCookie) > newVal )
    Cookies.set(nomCookie, newVal, { path: '' });
}

function majCookieExpert(newVal){
  let nomCookie = dicoCookies.get(30);
  if( Cookies.get(nomCookie) == undefined || Cookies.get(nomCookie) > newVal )
    Cookies.set(dicoCookies.get(nomCookie), newVal, { path: '' });
}
