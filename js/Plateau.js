class Plateau{
  constructor(hauteur, largeur, nbBombeVoulu) {
    this.hauteur = hauteur;
    this.largeur = largeur;
    this.valeurs = Plateau.genererBombe(nbBombeVoulu, hauteur*largeur);
    this.nbCaseATeste = hauteur*largeur - nbBombeVoulu;
    this.nbrBombeVoulu = nbBombeVoulu
  }

  static genererBombe(nbBombeVoulu, taille){
    let res = new Array(taille);
    let nbBombePlace = 0;
    for(let i=0; i<taille; ++i){
      if ( Math.random() < (nbBombeVoulu-nbBombePlace)/(taille-i) ){
        ++nbBombePlace;
        res[i] = true;
      }
      else{
        res[i] = false;
      }
    }
    return res;
  }

  get(x, y){
    return this.valeurs[ x + this.largeur*y ];
  }

  valCase(x, y){
    if( this.get(x, y) ){
      return -1;
    }

    let res = 0;
    for(let i=Math.max(0, x-1); i<= Math.min(x+1, this.largeur-1); ++i){
      for(let j=Math.max(0, y-1); j<=Math.min(y+1, this.hauteur-1); ++j){
        res += this.get(i, j);
      }
    }
    return res;
  }
}
