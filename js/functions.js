function afficherPlateau(hauteur, largeur){ // affiche le plateau actuel
  $("#plateau").empty();
  for(let j=0; j<hauteur; ++j){
    let ligne = document.createElement("tr");
    for(let i=0; i<largeur; ++i){
      let caseTab = document.createElement("td");
      caseTab.append( BoutonLib.creerBouton(i, j) );
      ligne.append(caseTab);
    }
    $("#plateau").append(ligne);
  }
}

function bloquer(){ // bloque le plateau
  clearInterval(idChrono);
  idChrono = null;

  let btns = $(".case");
  for(elem of btns){
    $(elem).prop( "onclick", null )
  }
}

function modalFin(victoire){
  if(victoire){
    let tps = $("#timeur").text()
    $("#modalTitre").text(`Victoire en ${tps} secondes !`);
    cree
  }
  else{
    $("#modalTitre").text("Défaite !");
  }
  let cmd = `init(${tableauBombes.hauteur}, ${tableauBombes.largeur}, ${tableauBombes.nbrBombeVoulu})`;
  $("#modalBtn").attr("onclick", cmd);
  $('#modalFin').modal('open');
}

function propagationVide(x, y){ // propage les cases 0
  for(let i=Math.max(0, x-1); i<= Math.min(x+1, tableauBombes.largeur-1); ++i){
    for(let j=Math.max(0, y-1); j<=Math.min(y+1, tableauBombes.hauteur-1); ++j){
      if ( i!=x || j!=y ){
        let btn = BoutonLib.getBouton(i, j);
        if ( BoutonLib.getEtat(btn) == "base" ){
          let valCase = tableauBombes.valCase(i, j);
          EtatsLib.setDesac( btn, valCase );
          if( valCase == 0 ){
            propagationVide(i, j)
          }
        }
      }
    }
  }
}

function clicBtn(bouton){ // fonction lancé au clic sur un boutton du plateau
  let pos = BoutonLib.getPosition(bouton);
  let rep = tableauBombes.valCase(pos[0], pos[1]);

  if ( rep == -1 ){ // si perdu
    EtatsLib.setBombe(bouton);
    bloquer();
    modalFin(false);
  }
  else{
    let fin = EtatsLib.setDesac(bouton, rep);
    if (!fin && rep == 0){ // si non fini et case sans bombe autour
      propagationVide(pos[0], pos[1]);
    }
    if ( tableauBombes.nbCaseATeste == 0 ){ // test de victoire
      bloquer();
      modalFin(true);
    }
  }
  if( idChrono == null ){
    $("#timeur").text("0");
    idChrono = setInterval(
      function(){
        $("#timeur").text( Number( $("#timeur").text() ) + 1 );
      },
      1000
    );
  }
}

function chargerRecords(){
  let deb = Cookies.get( dicoCookies.get(9) );
  let inter = Cookies.get( dicoCookies.get(16) );
  let exp = Cookies.get( dicoCookies.get(30) );

  if(deb != undefined)
    $("#recordDeb").text(" "+deb);
  else
    $("#recordDeb").text(" NA");

  if(inter != undefined)
    $("#recordInter").text(" "+inter);
    else
      $("#recordInter").text(" NA");

  if(exp != undefined)
    $("#recordExp").text(" "+exp);
    else
      $("#recordExp").text(" NA");
}
