// appels Materialize
$(document).ready(function(){
  $('.tooltipped').tooltip();
});

$(document).ready(function(){
  $('.modal').modal();
});

// variables globales
tableauBombes = 0;
idChrono = null;

// fonction de création du jeu
function init(hauteur, largeur, nbBombeVoulu){
  clearInterval(idChrono);
  idChrono = null;
  $("#timeur").text("0");

  tableauBombes = new Plateau(hauteur, largeur, nbBombeVoulu);
  console.log(tableauBombes);
  afficherPlateau(hauteur, largeur);
}

init(9, 9, 10);
chargerRecords();
